# mern-demo

This project demonstrates running a Node.js backend and a React frontend app

## Running the Application

To run the application, make sure you have Docker and Docker Compose installed on your system. Then, follow these steps:

1. Clone this repository:

   ```bash
   git clone https://gitlab.com/paras.dudhat/mern-demo.git


   ```

2. Change to the project directory:

   ```bash
    cd mern-demo

   ```

   This command will build and start the containers in detached mode.

3. Build and start the containers:
   ```bash
   docker-compose up -d
   ```
4. Access the application:

   Once the containers are up and running, you can access the application in your web browser at:

   - Frontend: http://localhost:3000
   - Backend: http://localhost:4000
