let i18n = require("i18n");
i18n.configure({
  defaultLocale: "en",
  // where to store json files - defaults to './locales' relative to modules directory
  directory: __dirname + "/locales",
  // setup some locales - other locales default to en silently
  locales: "en",
  // sets a custom cookie name to parse locale settings from - defaults to NULL
  cookie: "certiadmin",
  // watch for changes in json files to reload locale on updates - defaults to false
  autoReload: true,
  // enable object notation
  objectNotation: true,

  // setting of log level DEBUG - default to require('debug')('i18n:debug')
  logDebugFn: function (msg) {
    console.log("debug", msg);
  },

  // setting of log level WARN - default to require('debug')('i18n:warn')
  logWarnFn: function (msg) {
    console.log("warn", msg);
  },

  // setting of log level ERROR - default to require('debug')('i18n:error')
  logErrorFn: function (msg) {
    console.log("error", msg);
  },
});

module.exports = i18n;
