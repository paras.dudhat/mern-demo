"use strict";

const mongoose = require("mongoose");

mongoose.connect(process.env.DATA_BASE_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;

db.on("error", (error) => {
  console.error("Exception occurred during database connection", error);
  throw error;
});

db.once("open", () => {
  console.log("Database connected successfully");
});

module.exports = db;
