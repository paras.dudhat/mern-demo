const { StatusCodes } = require("http-status-codes");
module.exports = {
  createValidationResponse(res, errors) {
    module.exports.createResponse(
      res,
      StatusCodes.UNPROCESSABLE_ENTITY,
      errors[Object.keys(errors)[0]],
      { ...errors },
      {}
    );
  },
  createResponse(res, status, message, payload) {
    return res.status(status).json({
      status,
      message,
      payload,
    });
  },
  isEmpty(value) {
    if (
      value === undefined ||
      value === null ||
      typeof value === "undefined" ||
      (typeof value === "object" && Object.keys(value).length === 0) ||
      (typeof value === "string" && value.trim().length === 0)
    ) {
      return true;
    } else {
      return false;
    }
  },
  isNumber(value) {
    const number = value;
    const myRegEx = /^(\s*[0-9]+\s*)+$/;
    const isValid = myRegEx.test(number);
    if (isValid) {
      return true;
    } else {
      return false;
    }
  },
};
