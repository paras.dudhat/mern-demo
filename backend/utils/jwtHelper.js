const helper = require("./helper.js");
const { HttpStatusCode } = require("axios");
const jwt = require("jsonwebtoken");

module.exports = {
  /**
   * @param {Object} data
   */
  createToken({ user }) {
    try {
      const expiresIn = process.env.JWT_EXPIRES_IN;
      const secret = process.env.JWT_SECRET;
      let dataStoredInToken = {
        userId: user._id,
        email: user.email,
        phoneNumber: user.phoneNumber,
      };
      return {
        expiresIn,
        token: jwt.sign(dataStoredInToken, secret, { expiresIn }),
      };
    } catch (error) {
      logger.error(__filename, {
        custom_message: "An error occurred while generating JWT token",
        error,
      });
      throw error;
    }
  },

  async authenticateToken(req, res, next) {
    try {
      const token = req.headers.authorization;
      if (token) {
        jwt.verify(
          token.split(" ").pop(),
          process.env.JWT_SECRET,
          async (err, decoded) => {
            if (err) {
              return helper.createResponse(
                res,
                HttpStatusCode.Unauthorized,
                res["__"](err.message),

                // "Unauthorized access",
                {}
              );
            }
            req.body.tokenData = decoded;
            next();
          }
        );
      } else {
        helper.createResponse(
          res,
          HttpStatusCode.Unauthorized,
          "Token required",
          {}
        );
        return;
      }
    } catch (error) {
      return helper.createResponse(
        res,
        HttpStatusCode.Unauthorized,
        "Unauthorized access",
        {}
      );
    }
  },
};
