const express = require("express");
const app = express();
require("dotenv").config();
const cors = require("cors");
const i18n = require("./i18n");

const port = process.env.PORT;

app.use(express.json());
app.use(cors());
app.use(i18n.init);
sequelizeObj = module.exports = require("./utils/sequelize");
app.get("/health", (req, res) => {
  res.status(200).send("healthy");
});
app.get("/test", (req, res) => {
  res.status(200).send("test success");
});

/**
 * app routers
 */
const userRouter = require("./app/userComponent/v1/userRouter");
app.use("/user/api/v1", userRouter);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

process.on("uncaughtException", (err) => {
  console.error(
    "process.on(uncaughtException)",
    new Date().toUTCString() + "uncaughtException:",
    err.message
  );
  console.error(__filename, "process.on(uncaughtException)", err.stack);
  process.exit(0);
});
