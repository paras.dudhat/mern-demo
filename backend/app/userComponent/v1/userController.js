const helper = require("../../../utils/helper");
const { StatusCodes } = require("http-status-codes");
const UserRecord = require("../models/userModel");
const jwtHelper = require("../../../utils/jwtHelper");
const { HttpStatusCode } = require("axios");
const bcrypt = require("bcrypt");

class UserController {
  /**
   * user registration
   * @param {*} req
   * @param {*} res
   */
  async userRegister(req, res) {
    try {
      const { firstName, lastName, email, password, phoneNumber } = req.body;

      let record = await UserRecord.findOne({ email }).lean();
      if (record) {
        helper.createResponse(
          res,
          HttpStatusCode.UnprocessableEntity,
          res["__"]("USER.EMAIL_EXIST"),
          {}
        );
        return;
      }
      const encryptedPassword = await bcrypt.hash(password, 10);
      let user = await UserRecord.create({
        firstName,
        lastName,
        email,
        phoneNumber,
        password: encryptedPassword,
      });

      helper.createResponse(
        res,
        StatusCodes.OK,
        res["__"]("USER.REGISTRATION_SUCCESS")
      );
    } catch (error) {
      console.log("Error During register user", error);
      helper.createResponse(
        res,
        error.response.data.code || StatusCodes.INTERNAL_SERVER_ERROR,
        error.response.data.error_message || "something went wrong"
      );
    }
  }

  /**
   * user login with email and password
   * @param {*} req
   * @param {*} res
   */
  async userLogin(req, res) {
    const { email, password } = req.body;

    let record = await UserRecord.findOne({ email }).lean();
    if (!record) {
      helper.createResponse(
        res,
        HttpStatusCode.UnprocessableEntity,
        res["__"]("USER.DATA_NOT_FOUND"),
        {}
      );
      return;
    }
    const isPwdMatching = await bcrypt.compare(password, record.password);

    if (!isPwdMatching) {
      helper.createResponse(
        res,
        HttpStatusCode.UnprocessableEntity,
        res["__"]("USER.INVALID_CREDENTIALS"),
        {}
      );
      return;
    } else {
      const tokenData = jwtHelper.createToken({ user: record });
      helper.createResponse(
        res,
        StatusCodes.OK,
        res["__"]("USER.LOGIN_SUCCESS"),
        {
          data: {
            firstName: record.firstName,
            lastName: record.lastName,
            userId: record._id,
            email: record.email,
            phoneNumber: record.phoneNumber,
          },
          token: tokenData.token,
        }
      );
    }
  }
  catch(error) {
    console.log("Error During login user", error);
    helper.createResponse(
      res,
      error.response.data.code || StatusCodes.INTERNAL_SERVER_ERROR,
      error.response.data.error_message || "something went wrong"
    );
  }

  /**
   * get user profit
   * @param {*} req
   * @param {*} res
   */
  async userProfile(req, res) {
    const { tokenData } = req.body;

    let record = await UserRecord.findOne({ email: tokenData.email }).lean();
    if (!record) {
      helper.createResponse(
        res,
        HttpStatusCode.UnprocessableEntity,
        res["__"]("USER.DATA_NOT_FOUND"),
        {}
      );
      return;
    }

    helper.createResponse(
      res,
      StatusCodes.OK,
      res["__"]("USER.PROFILE_SUCCESS"),
      {
        data: {
          firstName: record.firstName,
          lastName: record.lastName,
          userId: record._id,
          email: record.email,
          phoneNumber: record.phoneNumber,
        },
      }
    );
  }

  catch(error) {
    console.log("Error During user profile", error);
    helper.createResponse(
      res,
      error.response.data.code || StatusCodes.INTERNAL_SERVER_ERROR,
      error.response.data.error_message || "something went wrong"
    );
  }
}
const controllerObj = new UserController();
module.exports = controllerObj;
