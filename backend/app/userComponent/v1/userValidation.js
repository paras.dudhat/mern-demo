const {
  createValidationResponse,
  isEmpty,
  isNumber,
} = require("../../../utils/helper");

class UserValidation {
  userRegister(req, res, next) {
    let errors = {};
    const {
      firstName,
      lastName,
      email,
      password,
      phoneNumber,
      confirmPassword,
    } = req.body;
    if (isEmpty(firstName)) {
      errors["firstName"] = res["__"]("VALIDATIONS.required", "firstName");
    }
    if (isEmpty(email)) {
      errors["email"] = res["__"]("VALIDATIONS.required", "email");
    }
    if (isEmpty(phoneNumber)) {
      errors["phoneNumber"] = res["__"]("VALIDATIONS.required", "phoneNumber");
    } else {
      if (!isNumber(phoneNumber)) {
        errors["phoneNumber"] = res["__"](
          "VALIDATIONS.mustNumber",
          "phoneNumber"
        );
      }
    }
    if (isEmpty(password)) {
      errors["password"] = res["__"]("VALIDATIONS.required", "password");
    } else if (isEmpty(confirmPassword)) {
      errors["confirmPassword"] = res["__"](
        "VALIDATIONS.required",
        "confirmPassword"
      );
    } else if (password !== confirmPassword) {
      errors["confirmPassword"] = res["__"](
        "VALIDATIONS.notMatch",
        "password",
        "confirmPassword"
      );
    }

    if (Object.keys(errors).length > 0) {
      createValidationResponse(res, errors);
    } else {
      next();
    }
  }

  userLogin(req, res, next) {
    let errors = {};
    const { email, password } = req.body;
    if (isEmpty(email)) {
      errors["email"] = res["__"]("VALIDATIONS.required", "email");
    }
    if (isEmpty(password)) {
      errors["password"] = res["__"]("VALIDATIONS.required", "password");
    }

    if (Object.keys(errors).length > 0) {
      createValidationResponse(res, errors);
    } else {
      next();
    }
  }
  userProfile(req, res, next) {
    let errors = {};

    const {} = req.body;
    const { authorization } = req.headers;

    if (isEmpty(authorization)) {
      errors["authorization"] = res["__"](
        "VALIDATIONS.required",
        "authorization"
      );
    }
    if (Object.keys(errors).length > 0) {
      createValidationResponse(res, errors);
    } else {
      next();
    }
  }
}
const validationObj = new UserValidation();
module.exports = validationObj;
