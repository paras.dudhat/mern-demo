const express = require("express");
const router = express.Router();

const userController = require("./userController");
const userValidation = require("./userValidation");
const jwtHelper = require("../../../utils/jwtHelper");
router.post("/register", userValidation.userRegister, (req, res, next) => {
  userController.userRegister(req, res);
});
router.post("/login", userValidation.userLogin, (req, res, next) => {
  userController.userLogin(req, res);
});
router.get(
  "/profile",
  userValidation.userProfile,
  jwtHelper.authenticateToken,
  (req, res, next) => {
    userController.userProfile(req, res);
  }
);

module.exports = router;
