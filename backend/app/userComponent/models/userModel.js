"use strict";

const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const UserSchema = new Schema(
  {
    firstName: { type: String, required: false },
    lastName: { type: String, required: false },
    email: { type: String, required: true },
    password: { type: String },
    phoneNumber: { type: String, required: false },
  },
  { timestamps: true }
);

const UserRecord = mongoose.model("User", UserSchema);

module.exports = UserRecord;
