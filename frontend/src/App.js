import "./App.css";
import MainPage from "./Routes/MainPage";
import ProtectedRouts from "./Routes/ProtectedRouts";
import Login from "./components/Login/Login";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Register from "./components/auth/register/register";
import Profile from "./components/Profile/Profile";

function App() {
  return (
    <>
      <Router>
        <div style={{ marginTop: "55px" }}>
          <Routes>
            <Route element={<MainPage />} path="/">
              <Route element={<ProtectedRouts />}>
                <Route index element={<Profile />} />
              </Route>
            </Route>
            <Route element={<Login />} path="/login" />
            <Route element={<Register />} path="/register" />
            <Route
              element={<h1 style={{ marginTop: "50px" }}>Page not found</h1>}
              path="/*"
            />
          </Routes>
        </div>
      </Router>
    </>
  );
}

export default App;
