import React, { useEffect, useState } from "react";
import { getProfile } from "../Login/Apis/api";
import { useNavigate } from "react-router-dom";

const Profile = () => {
  const [data, setData] = useState({});
  const [error, setError] = useState("");
  const history = useNavigate();
  const GetProfile = async () => {
    try {
      const { data } = await getProfile();
      if (data.status === 200) {
        setData(data.payload.data);
      }
    } catch (error) {
      if (error?.response?.data?.status === 401) {
        setError(error.response.data.message);
        history("/login");
        localStorage.removeItem("token");
      } else if (error?.response?.data?.message) {
        setError(error.response.data.message);
      } else {
        setError("An error occurred during sign in.");
      }
    }
  };

  useEffect(() => {
    document.title = "Profile";
    GetProfile();
  }, []);
  return (
    <div className="container">
      {error && (
        <div className="alert alert-danger" style={{ marginTop: "1rem" }}>
          {error}
        </div>
      )}
      <div className=" d-flex justify-content-center pt-5">
        <div className="card" style={{ width: "18rem" }}>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">
              {"first Name : " + data.firstName}
            </li>
            <li className="list-group-item">
              {"last Name : " + data.lastName}
            </li>
            <li className="list-group-item">
              {"phone Number : " + data.phoneNumber}
            </li>
            <li className="list-group-item">{"email : " + data.email}</li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Profile;
