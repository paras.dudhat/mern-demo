import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { login, setAxiosConfig } from "./Apis/api";
const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const history = useNavigate();

  useEffect(() => {
    document.title = "Login";
  }, []);

  const handleSignIn = async (e) => {
    e.preventDefault();
    let formData = {
      email,
      password,
    };
    try {
      const { data } = await login(formData);
      if (data.status === 200) {
        localStorage.setItem("token", data.payload.token);
        setAxiosConfig();

        // Redirect to the home page
        history("/");
      }
    } catch (error) {
      if (error?.response?.data?.message) {
        setError(error.response.data.message);
      } else {
        setError("An error occurred during sign in.");
      }
    }
  };
  return (
    <div className="container">
      {error && <div className="alert alert-danger">{error}</div>}
      <h1>Sign In</h1>
      <form onSubmit={handleSignIn}>
        <div className="mb-3">
          <label htmlFor="email" className="form-label">
            Email
          </label>
          <input
            type="email"
            className="form-control"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="password" className="form-label">
            Password
          </label>
          <input
            type="password"
            className="form-control"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </div>
        <div className="d-flex" style={{ gap: "5px" }}>
          <div>
            <button type="submit" className="btn btn-primary">
              Sign In
            </button>
          </div>
          <div>
            <Link type="button" className="btn btn-primary" to={"/register"}>
              Register
            </Link>
          </div>
        </div>
      </form>
    </div>
  );
};
export default Login;
