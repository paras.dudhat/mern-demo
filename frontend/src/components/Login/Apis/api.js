import axios from "axios";

// Function to handle common API headers and configurations
let apiClient = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: {
    "Content-Type": "application/json",
    Authorization: `Bearer ${localStorage.getItem("token")}`,
  },
});

export const setAxiosConfig = () => {
  apiClient = axios.create({
    baseURL: process.env.REACT_APP_API_URL,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
};

// API endpoints
export const endpoints = {
  login: "/user/api/v1/login",
  register: "/user/api/v1/register",
  getData: "/user/api/v1/Profile",
};

// Function to make a login API call
export const login = (formData) => {
  return apiClient.post(endpoints.login, formData);
};
export const register = (formData) => {
  return apiClient.post(endpoints.register, formData);
};

// Function to make a data retrieval API call
export const getProfile = () => {
  return apiClient.get(endpoints.getData);
};
