import React, { useEffect, useState } from "react";
import { login, register, setAxiosConfig } from "../../Login/Apis/api";
import { Link, useNavigate } from "react-router-dom";

const Register = () => {
  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    phoneNumber: "",
    email: "",
    password: "",
    confirmPassword: "",
  });
  const [error, setError] = useState("");
  const history = useNavigate();
  const [successMsg, setSuccessMsg] = useState("");

  const handleValueChange = (e) => {
    let { name, value } = e.target;

    setFormData((prev) => {
      return {
        ...prev,
        [name]: value,
      };
    });
  };

  const handleRegister = async (e) => {
    e.preventDefault();

    try {
      //   const response = await axios.post("/login", { phoneNumber, password });
      const { data } = await register(formData);
      if (data.status === 200) {
        setAxiosConfig();
        setSuccessMsg(data.message);
        setError("");
        // Redirect to the home page
        setTimeout(() => {
          history("/login");
        }, 1500);
      }
      // Do something with the response, such as storing tokens or redirecting to a new page
    } catch (error) {
      if (error?.response?.data?.message) {
        setError(error.response.data.message);
      } else {
        setError("An error occurred during sign in.");
      }
    }
  };

  const checkPhone = (object) => {
    if (object.target.value.length > object.target.maxLength) {
      object.target.value = object.target.value.slice(
        0,
        object.target.maxLength
      );
    }
  };
  useEffect(() => {
    document.title = "Registration";
  }, []);
  return (
    <>
      <div className="container">
        {error && <div className="alert alert-danger">{error}</div>}
        {successMsg && <div className="alert alert-success">{successMsg}</div>}
        <h1>Register</h1>
        <form onSubmit={handleRegister}>
          <div className="mb-3">
            <label for="exampleInputEmail1" className="form-label">
              First Name
            </label>
            <input
              type="text"
              name="firstName"
              value={formData.firstName}
              onChange={(e) => handleValueChange(e)}
              className="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              required
            />
          </div>
          <div className="mb-3">
            <label for="exampleInputEmail1" className="form-label">
              Last Name
            </label>
            <input
              type="text"
              name="lastName"
              value={formData.lastName}
              onChange={(e) => handleValueChange(e)}
              className="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
            />
          </div>
          <div className="mb-3">
            <label for="exampleInputEmail1" className="form-label">
              Phone Number
            </label>
            <input
              type="number"
              name="phoneNumber"
              value={formData.phoneNumber}
              min={0}
              maxLength={10}
              onInput={checkPhone}
              onChange={(e) => handleValueChange(e)}
              className="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              required
            />
          </div>
          <div className="mb-3">
            <label for="exampleInputEmail1" className="form-label">
              Email address
            </label>
            <input
              type="email"
              name="email"
              value={formData.email}
              onChange={(e) => handleValueChange(e)}
              className="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              required
            />
          </div>
          <div className="mb-3">
            <label for="exampleInputPassword1" className="form-label">
              Password
            </label>
            <input
              type="password"
              name="password"
              value={formData.password}
              onChange={(e) => handleValueChange(e)}
              className="form-control"
              id="exampleInputPassword1"
              required
            />
          </div>
          <div className="mb-3">
            <label htmlFor="password" className="form-label">
              Confirm Password
            </label>
            <input
              type="password"
              className="form-control"
              id="password"
              name="confirmPassword"
              value={formData.confirmPassword}
              onChange={(e) => handleValueChange(e)}
              required
            />
          </div>
          <div className="d-flex" style={{ gap: "5px" }}>
            <div>
              <button type="submit" className="btn btn-primary">
                Register
              </button>
            </div>
            <div>
              <Link type="button" className="btn btn-primary" to={"/login"}>
                Sign In
              </Link>
            </div>
          </div>
        </form>
      </div>
    </>
  );
};

export default Register;
