import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { setAxiosConfig } from "../Login/Apis/api";

const Navbar = ({ title, home }) => {
  const changeTitle = (title_name) => {
    document.title = title_name;
  };
  const history = useNavigate();
  const logoutHandel = () => {
    localStorage.removeItem("token");
    setAxiosConfig();
    history("/Login");
  };
  return (
    <nav className="navbar navbar-expand-lg bg-body-tertiary fixed-top">
      <div className="container-fluid">
        <Link className="navbar-brand" to="/">
          {title}
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <Link
                className="nav-link active"
                aria-current="page"
                to="/"
                onClick={() => changeTitle(home)}
              >
                {home}
              </Link>
            </li>
          </ul>

          <button
            className="btn btn-outline-success"
            type="submit"
            onClick={logoutHandel}
          >
            Logout
          </button>
        </div>
      </div>
    </nav>
  );
};
export default Navbar;
