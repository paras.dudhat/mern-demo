import React, { Fragment } from "react";
import Navbar from "../components/NavBar/Navbar";
import { Outlet } from "react-router-dom";

const MainPage = () => {
  return (
    <Fragment>
      <Navbar title="demo-app" home="Profile" />
      <Outlet />
    </Fragment>
  );
};

export default MainPage;
