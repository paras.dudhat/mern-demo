import React from "react";
import { Navigate, Outlet } from "react-router-dom";

const ProtectedRouts = () => {
  return localStorage.getItem("token") ? (
    <>
      <Outlet />
    </>
  ) : (
    <Navigate to="/login" />
  );
};

export default ProtectedRouts;
